package edu.fullstack.tareafullstack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TareafullstackApplication {

    public static void main(String[] args) {
        SpringApplication.run(TareafullstackApplication.class, args);
    }
}
