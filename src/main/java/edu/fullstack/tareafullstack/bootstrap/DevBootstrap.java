package edu.fullstack.tareafullstack.bootstrap;

import edu.fullstack.tareafullstack.domain.BuyOrder;
import edu.fullstack.tareafullstack.domain.Incident;
import edu.fullstack.tareafullstack.domain.IncidentRegistry;
import edu.fullstack.tareafullstack.repositories.BuyOrderRepository;
import edu.fullstack.tareafullstack.repositories.IncidentRegistryRepository;
import edu.fullstack.tareafullstack.repositories.IncidentRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private BuyOrderRepository buyOrderRepository;
    private IncidentRepository incidentRepository;
    private IncidentRegistryRepository incidentRegistryRepository;

    public DevBootstrap(BuyOrderRepository buyOrderRepository, IncidentRepository incidentRepository, IncidentRegistryRepository incidentRegistryRepository) {
        this.buyOrderRepository = buyOrderRepository;
        this.incidentRepository = incidentRepository;
        this.incidentRegistryRepository = incidentRegistryRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {
        BuyOrder primera=new BuyOrder();
        primera.setCode("112233");
        primera.setName("Electronics");

        IncidentRegistry incidentReg=new IncidentRegistry();
        incidentReg.setCode("cod001");
        incidentRegistryRepository.save(incidentReg);

        Incident incident=new Incident();
        incident.setBuyOrder(primera);
        incident.setCode("cod_inc001");
        incident.setIncidentRegistry(incidentReg);

        primera.getIncidents().add(incident);
        buyOrderRepository.save(primera);
        incidentRepository.save(incident);
//        primera=buyOrderRepository.findById(primera.getId()).get();
//        primera.setName("Food");
//        buyOrderRepository.save(primera);
//        primera=buyOrderRepository.findById(primera.getId()).get();
//        primera.setName("Clothes");
//        buyOrderRepository.save(primera);
    }
}
