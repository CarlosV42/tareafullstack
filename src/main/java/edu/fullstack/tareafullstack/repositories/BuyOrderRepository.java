package edu.fullstack.tareafullstack.repositories;

import edu.fullstack.tareafullstack.domain.BuyOrder;
import org.springframework.data.repository.CrudRepository;

public interface BuyOrderRepository extends CrudRepository<BuyOrder, Long> {
}
