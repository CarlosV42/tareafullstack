package edu.fullstack.tareafullstack.repositories;

import edu.fullstack.tareafullstack.domain.Incident;
import org.springframework.data.repository.CrudRepository;

public interface IncidentRepository extends CrudRepository<Incident, Long> {
}
