package edu.fullstack.tareafullstack.repositories;

import edu.fullstack.tareafullstack.domain.IncidentRegistry;
import org.springframework.data.repository.CrudRepository;

public interface IncidentRegistryRepository extends CrudRepository<IncidentRegistry, Long> {
}
