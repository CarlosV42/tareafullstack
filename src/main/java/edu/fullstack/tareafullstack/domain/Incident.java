package edu.fullstack.tareafullstack.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Incident extends ModelBase {
    private String code;

    @ManyToOne(optional = false)
    private BuyOrder buyOrder;

    @OneToOne(optional = false)
    private IncidentRegistry incidentRegistry;

    public BuyOrder getBuyOrder() {
        return buyOrder;
    }

    public void setBuyOrder(BuyOrder buyOrder) {
        this.buyOrder = buyOrder;
    }

    public IncidentRegistry getIncidentRegistry() {
        return incidentRegistry;
    }

    public void setIncidentRegistry(IncidentRegistry incidentRegistry) {
        this.incidentRegistry = incidentRegistry;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
