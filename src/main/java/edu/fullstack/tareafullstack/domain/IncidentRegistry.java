package edu.fullstack.tareafullstack.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class IncidentRegistry extends ModelBase {
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
