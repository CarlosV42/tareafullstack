package edu.fullstack.tareafullstack.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class BuyOrder extends ModelBase {
    private String name;
    private String code;

    @OneToMany(mappedBy = "buyOrder", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private List<Incident> incidents=new ArrayList<>();

    public List<Incident> getIncidents() {
        return incidents;
    }

    public void setIncidents(List<Incident> incidents) {
        this.incidents = incidents;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
