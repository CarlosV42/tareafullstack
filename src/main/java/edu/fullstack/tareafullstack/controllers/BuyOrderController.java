package edu.fullstack.tareafullstack.controllers;

import edu.fullstack.tareafullstack.services.BuyOrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BuyOrderController {
    private BuyOrderService buyOrderService;

    public BuyOrderController(BuyOrderService buyOrderService) {
        this.buyOrderService = buyOrderService;
    }

    @RequestMapping("/buyorders")
    public String getBuyOrders(Model model){
        model.addAttribute("buyorders", buyOrderService.findAll());
        return "buyorders";
    }
}
