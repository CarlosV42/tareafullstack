package edu.fullstack.tareafullstack.services;

import edu.fullstack.tareafullstack.domain.BuyOrder;

import java.util.List;

public interface BuyOrderService {
    List<BuyOrder> findAll();
}
