package edu.fullstack.tareafullstack.services;

import edu.fullstack.tareafullstack.domain.BuyOrder;
import edu.fullstack.tareafullstack.repositories.BuyOrderRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BuyOrderServiceImpl implements BuyOrderService {
    private BuyOrderRepository buyOrderRepository;

    public BuyOrderServiceImpl(BuyOrderRepository buyOrderRepository) {
        this.buyOrderRepository = buyOrderRepository;
    }

    @Override
    public List<BuyOrder> findAll() {
        List<BuyOrder> buyOrders=new ArrayList<>();
        buyOrderRepository.findAll().iterator().forEachRemaining(buyOrders::add);
        return buyOrders;
    }
}
